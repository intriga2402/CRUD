/**
 * validar usuario existente con ajax
 */

var usuarioExistente = false;
var emailExistente = false;

$("#usuarioRegistro").change(function(){

	var usuario = $("#usuarioRegistro").val();
	var datos = new FormData();
	datos.append("validarUsuario", usuario);
	
	$.ajax({
		url:"views/modules/ajax.php",
		method:"POST",
		data: datos,
		cache: false,
		contentType: false,
		processData: false,
		success:function(respuesta){

			if(respuesta == 0) {				
				$("label[for='usuarioRegistro'] span").html('<p>Este usuario ya existe en la base de datos</p>');
				usuarioExistente = true;				
			}else{
				$("label[for='usuarioRegistro'] span").html('');
				usuarioExistente = false;
			}
		}
	});

});

/**
 * validar email existente con ajax
 */
$("#emailRegistro").change(function(){

	var email = $("#emailRegistro").val();

	var datos = new FormData();
	datos.append("validarEmail", email);
	
	$.ajax({
		url:"views/modules/ajax.php",
		method:"POST",
		data: datos,
		cache: false,
		contentType: false,
		processData: false,
		success:function(respuesta){
			
			if(respuesta == 0){

				$("label[for='emailRegistro'] span").html('<p>Este email ya existe en la base de datos</p>');

				emailExistente = true;
			}

			else{

				$("label[for='emailRegistro'] span").html("");

				emailExistente = false;

			}
		
		}

	});

});


/**
 * validar registro
 */
function validarRegistro(){

	var usuario = document.querySelector("#usuarioRegistro").value;
	
	var password = document.querySelector("#passwordRegistro").value;
	
	var email = document.querySelector("#emailRegistro").value;

	var terminos = document.querySelector("#terminos").checked;
	
	/**
	 * validar usuario
	 */
	if (usuario != "") {
		var caracteres = usuario.length;
		var expresion = /^[a-zA-Z0-9]*$/;

		if (caracteres > 6) {
			document.querySelector("label[for='usuarioRegistro']").innerHTML += "<br>Escriba por favor menos de 6 caracteres.";

			return false;
		}	

		if (!expresion.test(usuario)) {
			document.querySelector("label[for='usuarioRegistro']").innerHTML += "<br>No escriba caracteres especiales.";

			return false;
		}	

		if (usuarioExistente) {
			document.querySelector("label[for='usuarioRegistro'] span").innerHTML = "<p>Este usuario ya existe en la base de datos</p>";
			return false;
		}
	}

	/**
	 * validar password
	 */

	if (password != "") {
		var caracteres = password.length;
		var expresion = /^[a-zA-Z0-9]*$/;

		if (caracteres < 6) {
			document.querySelector("label[for='passwordRegistro']").innerHTML += "<br>Escriba por favor mas de 6 caracteres.";

			return false;
		}	

		if (!expresion.test(password)) {
			document.querySelector("label[for='passwordRegistro']").innerHTML += "<br>No escriba caracteres especiales.";

			return false;
		}	
	}

	/**
	 * validar email
	 */

	if (email != "") {
		var expresion = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;

		if (!expresion.test(email)) {
			document.querySelector("label[for='emailRegistro']").innerHTML += "<br>No escriba caracteres especiales.";

			return false;
		}

		if (emailExistente) {
			document.querySelector("label[for='emailRegistro'] span").innerHTML = "<p>Este email ya existe en la base de datos</p>";
			return false;
		}		
	}

	/**
	 * validar terminos
	 */

	if (!terminos) {
		document.querySelector("form").innerHTML += "<br>Acepte terminos y condiciones.";

		document.querySelector("#usuarioRegistro").value = usuario;
	
		document.querySelector("#passwordRegistro").value = password;
		
		document.querySelector("#emailRegistro").value = email;

		return false;
	}

	return true;
}