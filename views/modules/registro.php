<h1>REGISTRO DE USUARIO</h1>

<form method="post" onsubmit="return validarRegistro()">
	
	<label for="usuarioRegistro">Usuario<span></span></label>
	<input type="text" placeholder="" maxlength="6" name="usuarioRegistro" id="usuarioRegistro" required>
	
	<label for="passwordRegistro">Contraseña</label>
	<input type="password" placeholder="" name="passwordRegistro" id="passwordRegistro" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" required>
	
	<label for="emailRegistro">Email<span></span></label>
	<input type="email" placeholder="" name="emailRegistro" id="emailRegistro" required>

	<input type="submit" value="Enviar">

	<p style="text-align: center"><input type="checkbox" id="terminos"><a href="">Acepta terminos y condiciones</a></p>

</form>

<?php 

	$registro = new MvcController();
	$registro -> registroUsuarioController();

	if (isset($_GET["action"])) {
		if ($_GET["action"] == "ok") {
			echo "Registro exitoso";
		}
	}

?>