<?php 

/**
 * extencion de clases: los obj pueden ser extendidos y pueden
 * heredar propiedades y metodos. Para definir una clase como
 * extencion, debo definir una clase padre, y se utiliza dentro
 * de una clase hija
 */

require_once "conexion.php";

class Datos extends conexion{

	/**
	 * Registro de usuarios
	 */
	public function registroUsuarioModel($datosModel, $tabla){

		/**
		 * prepare() prepara una sentencia SQL para ser ejecutada por
		 * el metodo PDOStatement::execute(). la sentecia SQL puede
		 * contener cero o mas marcadores de parametros con nombre (:name) 
		 * o signos de interrogacion (?) por los cuales los valores reales
		 * seran sustituidos cuando la sentencia sea ejecutada. Ayuda a 
		 * prevenir inyecciones SQL eliminando la senecidad de entrecomillar
		 * manualmente los parametros
		 */
		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (usuario, password, email) VALUES (:usuario, :password, :email)");
		/**
		 * bindParam() vincula una variable php a un parametro de sustitucion con nombre o signo de interrogacion
		 * correspondiente de la sentencia sql que fue usada para preparar la sentencia
		 */
		$stmt->bindParam(":usuario", $datosModel["usuario"], PDO::PARAM_STR); 
		$stmt->bindParam(":password", $datosModel["password"], PDO::PARAM_STR); 
		$stmt->bindParam(":email", $datosModel["email"], PDO::PARAM_STR); 
		if($stmt->execute()){
			return "success";
		}else{
			return "error";
		}
		$stmt->close();
	}

	/**
	 * Ingreso de usuarios
	 */
	public function ingresoUsuarioModel($datosModel, $tabla){

		$stmt = Conexion::conectar()->prepare("SELECT usuario, password, intentos FROM $tabla WHERE usuario = :usuario");
		$stmt->bindParam(":usuario", $datosModel["usuario"], PDO::PARAM_STR); 
		$stmt->execute();

		/**
		 * fetch() Obtiene una fila de un conjunto de resultados asociado al objeto PDOStatement
		 */
		return $stmt->fetch();
		$stmt->close();
	}

	/**
	 * intentos usuarios
	 */
	public function intentosUsuarioModel($datosModel, $tabla){
		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET intentos = :intentos WHERE usuario = :usuario");
		$stmt->bindParam(":intentos", $datosModel["actualizarIntentos"], PDO::PARAM_INT);
		$stmt->bindParam(":usuario", $datosModel["usuarioActual"], PDO::PARAM_STR);

		if($stmt->execute()){
			return "success";
		}else{
			return "error";
		}
		$stmt->close();
	}


	/**
	 * Vista de usuarios
	 */
	public function vistaUsuarioModel($tabla){

		$stmt = Conexion::conectar()->prepare("SELECT id, usuario, password, email FROM $tabla");
		$stmt->execute();
		/**
		 * fetchAll() Obtiene todas las filas de un conjunto de resultados asociado al objeto PDOStatement
		 */
		return $stmt->fetchAll();
		$stmt->close();
	}

	/**
	 * Editar usuarios
	 */
	public function editarUsuarioModel($datosModel, $tabla){
		$stmt = Conexion::conectar()->prepare("SELECT id, usuario, password, email FROM $tabla WHERE id = :id");
		$stmt->bindParam(":id", $datosModel, PDO::PARAM_INT); 
		$stmt->execute();		
		return $stmt->fetch();
		$stmt->close();
	}

	/**
	 * Actualizar usuarios
	 */
	public function actualizarUsuarioModel($datosModel, $tabla){
		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET usuario = :usuario, password = :password, email = :email WHERE id = :id");

		$stmt->bindParam(":usuario", $datosModel["usuario"], PDO::PARAM_STR); 
		$stmt->bindParam(":password", $datosModel["password"], PDO::PARAM_STR); 
		$stmt->bindParam(":email", $datosModel["email"], PDO::PARAM_STR); 
		$stmt->bindParam(":id", $datosModel["id"], PDO::PARAM_INT);

		if($stmt->execute()){
			return "success";
		}else{
			return "error";
		}
		$stmt->close();
	}

	/**
	 * Borrar usuarios
	 */
	public function borrarUsuarioModel($datosModel, $tabla){
		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id = :id");
		$stmt->bindParam(":id", $datosModel, PDO::PARAM_INT);
		if($stmt->execute()){
			return "success";
		}else{
			return "error";
		}
		$stmt->close();
	}

	/**
	 * validar usuario existente
	 */
	public function validarUsuarioModel($datosModel, $tabla){
		$stmt = Conexion::conectar()->prepare("SELECT usuario FROM $tabla WHERE usuario = :usuario");
		$stmt->bindParam(":usuario", $datosModel, PDO::PARAM_STR); 
		$stmt->execute();		
		return $stmt->fetch();
		$stmt->close();
	}

	/**
	 * validar email existente
	 */
	public function validarEmailModel($datosModel, $tabla){
		$stmt = Conexion::conectar()->prepare("SELECT email FROM $tabla WHERE email = :email");
		$stmt->bindParam(":email", $datosModel, PDO::PARAM_STR); 
		$stmt->execute();		
		return $stmt->fetch();
		$stmt->close();
	}
	
}